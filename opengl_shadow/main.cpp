#define _CRT_SECURE_NO_WARNINGS

#include <iostream>
#include <sstream>
#include <string>
#include <fstream>
#include <vector>
#include <stdio.h>
#include <chrono>

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include<glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include "lodepng.h"

using namespace std;

void error_callback(int error, const char* description)
{
	fprintf(stderr, "Error: %s\n", description);
}

struct WorldState {
	glm::vec3 camera_position;
	glm::vec3 camera_forward;
	glm::vec3 camera_up;

	double prev_mouse_x;
	double prev_mouse_y;
	decltype(std::chrono::system_clock::now()) prev_time;

	bool should_show_shadowmap;
};

GLuint LoadShaders(const char* vertex_file_path, const char* fragment_file_path) {

	// Create the shaders
	GLuint VertexShaderID = glCreateShader(GL_VERTEX_SHADER);
	GLuint FragmentShaderID = glCreateShader(GL_FRAGMENT_SHADER);

	// Read the Vertex Shader code from the file
	std::string VertexShaderCode;
	std::ifstream VertexShaderStream(vertex_file_path, std::ios::in);
	if (VertexShaderStream.is_open()) {
		std::stringstream sstr;
		sstr << VertexShaderStream.rdbuf();
		VertexShaderCode = sstr.str();
		VertexShaderStream.close();
	}
	else {
		printf("Impossible to open %s. Are you in the right directory ? Don't forget to read the FAQ !\n", vertex_file_path);
		getchar();
		return 0;
	}

	// Read the Fragment Shader code from the file
	std::string FragmentShaderCode;
	std::ifstream FragmentShaderStream(fragment_file_path, std::ios::in);
	if (FragmentShaderStream.is_open()) {
		std::stringstream sstr;
		sstr << FragmentShaderStream.rdbuf();
		FragmentShaderCode = sstr.str();
		FragmentShaderStream.close();
	}

	GLint Result = GL_FALSE;
	int InfoLogLength;

	// Compile Vertex Shader
	printf("Compiling shader : %s\n", vertex_file_path);
	char const* VertexSourcePointer = VertexShaderCode.c_str();
	glShaderSource(VertexShaderID, 1, &VertexSourcePointer, NULL);
	glCompileShader(VertexShaderID);

	// Check Vertex Shader
	glGetShaderiv(VertexShaderID, GL_COMPILE_STATUS, &Result);
	glGetShaderiv(VertexShaderID, GL_INFO_LOG_LENGTH, &InfoLogLength);
	if (InfoLogLength > 0) {
		std::vector<char> VertexShaderErrorMessage(InfoLogLength + 1);
		glGetShaderInfoLog(VertexShaderID, InfoLogLength, NULL, &VertexShaderErrorMessage[0]);
		printf("%s\n", &VertexShaderErrorMessage[0]);
	}

	// Compile Fragment Shader
	printf("Compiling shader : %s\n", fragment_file_path);
	char const* FragmentSourcePointer = FragmentShaderCode.c_str();
	glShaderSource(FragmentShaderID, 1, &FragmentSourcePointer, NULL);
	glCompileShader(FragmentShaderID);

	// Check Fragment Shader
	glGetShaderiv(FragmentShaderID, GL_COMPILE_STATUS, &Result);
	glGetShaderiv(FragmentShaderID, GL_INFO_LOG_LENGTH, &InfoLogLength);
	if (InfoLogLength > 0) {
		std::vector<char> FragmentShaderErrorMessage(InfoLogLength + 1);
		glGetShaderInfoLog(FragmentShaderID, InfoLogLength, NULL, &FragmentShaderErrorMessage[0]);
		printf("%s\n", &FragmentShaderErrorMessage[0]);
	}

	// Link the program
	printf("Linking program\n");
	GLuint ProgramID = glCreateProgram();
	glAttachShader(ProgramID, VertexShaderID);
	glAttachShader(ProgramID, FragmentShaderID);
	glLinkProgram(ProgramID);

	// Check the program
	glGetProgramiv(ProgramID, GL_LINK_STATUS, &Result);
	glGetProgramiv(ProgramID, GL_INFO_LOG_LENGTH, &InfoLogLength);
	if (InfoLogLength > 0) {
		std::vector<char> ProgramErrorMessage(InfoLogLength + 1);
		glGetProgramInfoLog(ProgramID, InfoLogLength, NULL, &ProgramErrorMessage[0]);
		printf("%s\n", &ProgramErrorMessage[0]);
	}

	glDetachShader(ProgramID, VertexShaderID);
	glDetachShader(ProgramID, FragmentShaderID);

	glDeleteShader(VertexShaderID);
	glDeleteShader(FragmentShaderID);

	return ProgramID;
}


bool loadOBJ(
	const char* path,
	std::vector<glm::vec3>& out_vertices,
	std::vector<glm::vec2>& out_uvs,
	std::vector<glm::vec3>& out_normals
) {
	printf("Loading OBJ file %s...\n", path);

	std::vector<unsigned int> vertexIndices, uvIndices, normalIndices;
	std::vector<glm::vec3> temp_vertices;
	std::vector<glm::vec2> temp_uvs;
	std::vector<glm::vec3> temp_normals;


	FILE* file = fopen(path, "r");
	if (file == NULL) {
		printf("Impossible to open the file ! Are you in the right path ? See Tutorial 1 for details\n");
		getchar();
		return false;
	}

	while (1) {

		char lineHeader[128];
		// read the first word of the line
		int res = fscanf(file, "%s", lineHeader);
		if (res == EOF)
			break; // EOF = End Of File. Quit the loop.

		// else : parse lineHeader

		if (strcmp(lineHeader, "v") == 0) {
			glm::vec3 vertex;
			fscanf(file, "%f %f %f\n", &vertex.x, &vertex.y, &vertex.z);
			temp_vertices.push_back(vertex);
		}
		else if (strcmp(lineHeader, "vt") == 0) {
			glm::vec2 uv;
			fscanf(file, "%f %f\n", &uv.x, &uv.y);
			uv.y = -uv.y; // Invert V coordinate since we will only use DDS texture, which are inverted. Remove if you want to use TGA or BMP loaders.
			temp_uvs.push_back(uv);
		}
		else if (strcmp(lineHeader, "vn") == 0) {
			glm::vec3 normal;
			fscanf(file, "%f %f %f\n", &normal.x, &normal.y, &normal.z);
			temp_normals.push_back(normal);
		}
		else if (strcmp(lineHeader, "f") == 0) {
			std::string vertex1, vertex2, vertex3;
			unsigned int vertexIndex[3], uvIndex[3], normalIndex[3];
			int matches = fscanf(file, "%d/%d/%d %d/%d/%d %d/%d/%d\n", &vertexIndex[0], &uvIndex[0], &normalIndex[0], &vertexIndex[1], &uvIndex[1], &normalIndex[1], &vertexIndex[2], &uvIndex[2], &normalIndex[2]);
			if (matches != 9) {
				printf("File can't be read by our simple parser :-( Try exporting with other options\n");
				fclose(file);
				return false;
			}
			vertexIndices.push_back(vertexIndex[0]);
			vertexIndices.push_back(vertexIndex[1]);
			vertexIndices.push_back(vertexIndex[2]);
			uvIndices.push_back(uvIndex[0]);
			uvIndices.push_back(uvIndex[1]);
			uvIndices.push_back(uvIndex[2]);
			normalIndices.push_back(normalIndex[0]);
			normalIndices.push_back(normalIndex[1]);
			normalIndices.push_back(normalIndex[2]);
		}
		else {
			// Probably a comment, eat up the rest of the line
			char stupidBuffer[1000];
			fgets(stupidBuffer, 1000, file);
		}

	}

	// For each vertex of each triangle
	for (unsigned int i = 0; i < vertexIndices.size(); i++) {

		// Get the indices of its attributes
		unsigned int vertexIndex = vertexIndices[i];
		unsigned int uvIndex = uvIndices[i];
		unsigned int normalIndex = normalIndices[i];

		// Get the attributes thanks to the index
		glm::vec3 vertex = temp_vertices[vertexIndex - 1];
		glm::vec2 uv = temp_uvs[uvIndex - 1];
		glm::vec3 normal = temp_normals[normalIndex - 1];

		// Put the attributes in buffers
		out_vertices.push_back(vertex);
		out_uvs.push_back(uv);
		out_normals.push_back(normal);

	}
	fclose(file);
	return true;
}

void handle_input(GLFWwindow* window, WorldState* world_state) {
	float mouse_sensitivity = 0.01f;
	float walk_speed = 300.0f;

	double new_mouse_x, new_mouse_y;
	glfwGetCursorPos(window, &new_mouse_x, &new_mouse_y);

	auto current_time = std::chrono::system_clock::now();
	float elapsed_time = (current_time - world_state->prev_time).count() * 0.00000000001f;

	world_state->prev_time = current_time;

	float diff_x = (new_mouse_x - world_state->prev_mouse_x) * mouse_sensitivity;
	float diff_y = (new_mouse_y - world_state->prev_mouse_y) * mouse_sensitivity;
	world_state->prev_mouse_x = new_mouse_x;
	world_state->prev_mouse_y = new_mouse_y;

	glm::mat4 forward_rotation_matrix = glm::rotate(glm::mat4(1.0f), diff_x, world_state->camera_up);
	glm::vec4 rotated_forward = forward_rotation_matrix * glm::vec4(world_state->camera_forward, 0.0f);

	world_state->camera_forward[0] = rotated_forward[0];
	world_state->camera_forward[1] = rotated_forward[1];
	world_state->camera_forward[2] = rotated_forward[2];

	glm::vec3 right_vector = glm::cross(world_state->camera_forward, world_state->camera_up);

	if (glfwGetKey(window, GLFW_KEY_W)) {
		world_state->camera_position += world_state->camera_forward * elapsed_time * walk_speed;
	}
	if (glfwGetKey(window, GLFW_KEY_S)) {
		world_state->camera_position -= world_state->camera_forward * elapsed_time * walk_speed;
	}
	if (glfwGetKey(window, GLFW_KEY_A)) {
		world_state->camera_position += right_vector * elapsed_time * walk_speed;
	}
	if (glfwGetKey(window, GLFW_KEY_D)) {
		world_state->camera_position -= right_vector * elapsed_time * walk_speed;
	}
}

int main() {
	auto start_time = std::chrono::system_clock::now();
	int window_width = 1024;
	int window_height = 768;
	float fov = 2*3.1415f / 360.0f * 90.0f ;
	float aspect_ratio = static_cast<float>(window_width) / window_height;

	// a screen space rectangle on which we display the shadowmap texture
	vector<glm::vec2> depth_quad_vertices;
	vector<glm::vec2> depth_quad_uvs;
	depth_quad_vertices.push_back(glm::vec2(1.0f, 0.25f));
	depth_quad_vertices.push_back(glm::vec2(0.25f, 0.25f));
	depth_quad_vertices.push_back(glm::vec2(1.0f, 1.0f));
	depth_quad_vertices.push_back(glm::vec2(0.25f, 1.0f));

	depth_quad_uvs.push_back(glm::vec2(1.0f, 0.0f));
	depth_quad_uvs.push_back(glm::vec2(0.0f, 0.0f));
	depth_quad_uvs.push_back(glm::vec2(1.0f, 1.0f));
	depth_quad_uvs.push_back(glm::vec2(0.0f, 1.0f));

	
	vector<glm::vec3> ground_vertices;
	vector<glm::vec3> ground_normals;
	vector<glm::vec2> ground_uvs;

	ground_vertices.push_back(glm::vec3(10.0f, -1.0f, -10.0f));
	ground_vertices.push_back(glm::vec3(-10.0f, -1.0f, -10.0f));
	ground_vertices.push_back(glm::vec3(10.0f, -1.0f, 10.0f));
	ground_vertices.push_back(glm::vec3(-10.0f, -1.0f, 10.0f));

	ground_normals.push_back(glm::vec3(0.0f, 1.0f, 0.0f));
	ground_normals.push_back(glm::vec3(0.0f, 1.0f, 0.0f));
	ground_normals.push_back(glm::vec3(0.0f, 1.0f, 0.0f));
	ground_normals.push_back(glm::vec3(0.0f, 1.0f, 0.0f));

	ground_uvs.push_back(glm::vec2(2.0f, 0.0f));
	ground_uvs.push_back(glm::vec2(0.0f, 0.0f));
	ground_uvs.push_back(glm::vec2(2.0f, 2.0f));
	ground_uvs.push_back(glm::vec2(0.0f, 2.0f));


	vector<glm::vec3> monkey_vertices;
	vector<glm::vec2> monkey_uvs;
	vector<glm::vec3> monkey_normals;
	loadOBJ("shadows/textured_monkey.obj", monkey_vertices, monkey_uvs, monkey_normals);

	vector<unsigned char> raw_texture_file;
	vector<unsigned char> decoded_texture;
	unsigned int texture_width, texture_height;
	lodepng::load_file(raw_texture_file, "shadows/textured_monkey.png");
	lodepng::decode(decoded_texture, texture_width, texture_height, raw_texture_file);

	vector<unsigned char> ground_raw_texture_file;
	vector<unsigned char> ground_decoded_texture;
	unsigned int ground_texture_width, ground_texture_height;
	lodepng::load_file(ground_raw_texture_file, "shadows/wall_diffuse.png");
	lodepng::decode(ground_decoded_texture, ground_texture_width, ground_texture_height, ground_raw_texture_file);


	glewExperimental = true;
	if (glfwInit() != GLFW_TRUE) {
		cout << "could not initialize glfw" << endl;
		return -1;
	}

	glfwSetErrorCallback(error_callback);

	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);


	GLFWwindow* window = glfwCreateWindow(window_width, window_height, "Game Over!", nullptr, nullptr);
	if (!window) {
		cout << "could not create glfw window" << endl;
		glfwTerminate();
		return -1;
	}

	glfwMakeContextCurrent(window);

	if (glewInit() != GLEW_OK){
		cout << "could not initialize glew" << endl;
		return -1;
	}

	glfwSetInputMode(window, GLFW_STICKY_KEYS, GLFW_TRUE);

	// if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress)) {
	// 	cout << "could not load glad" << endl;
	// 	glfwTerminate();
	// 	return -1;
	// }


	//setup world state
	WorldState world_state;
	world_state.camera_position = glm::vec3(0.0f, 0.0f, 3.0f);
	world_state.camera_forward = glm::vec3(0.0f, 0.0f, -1.0f);
	world_state.camera_up = glm::vec3(0.0f, 1.0f, 0.0f);
	glfwGetCursorPos(window, &world_state.prev_mouse_x, &world_state.prev_mouse_y);
	world_state.prev_time = std::chrono::system_clock::now();
	world_state.should_show_shadowmap = false;

	glm::vec3 original_light_direction(0.0f, -2.0f, -4.0f);
	original_light_direction = glm::normalize(original_light_direction);


	glfwSetWindowUserPointer(window, &world_state);
	glfwSetKeyCallback(window, (GLFWkeyfun)[](GLFWwindow* window, int key, int scancode, int action, int mod) {
			WorldState* ws = (WorldState*)glfwGetWindowUserPointer(window);
			if (key == GLFW_KEY_M && action == GLFW_PRESS) {
				ws->should_show_shadowmap = !ws->should_show_shadowmap;
			}
		});



	GLuint monkey_vertex_array_object, ground_vertex_array_object, depth_quad_vertex_array_object;
	glGenVertexArrays(1, &monkey_vertex_array_object);
	glGenVertexArrays(1, &ground_vertex_array_object);
	glGenVertexArrays(1, &depth_quad_vertex_array_object);

	glBindVertexArray(monkey_vertex_array_object);

	GLuint monkey_vertex_buffer, monkey_normal_buffer, monkey_uv_buffer;
	glGenBuffers(1, &monkey_vertex_buffer);
	glGenBuffers(1, &monkey_normal_buffer);
	glGenBuffers(1, &monkey_uv_buffer);

	glBindBuffer(GL_ARRAY_BUFFER, monkey_vertex_buffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec3) * monkey_vertices.size(), &monkey_vertices[0], GL_STATIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);

	glBindBuffer(GL_ARRAY_BUFFER, monkey_normal_buffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec3) * monkey_normals.size(), &monkey_normals[0], GL_STATIC_DRAW);
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, 0);

	glBindBuffer(GL_ARRAY_BUFFER, monkey_uv_buffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec2) * monkey_uvs.size(), &monkey_uvs[0], GL_STATIC_DRAW);
	glEnableVertexAttribArray(2);
	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 0, 0);

	GLuint monkey_texture;
	glGenTextures(1, &monkey_texture);
	glBindTexture(GL_TEXTURE_2D, monkey_texture);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, texture_width, texture_height, 0, GL_RGBA, GL_UNSIGNED_BYTE, &decoded_texture[0]);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

	glBindVertexArray(ground_vertex_array_object);

	GLuint ground_vertex_buffer, ground_normal_buffer, ground_uv_buffer;
	glGenBuffers(1, &ground_vertex_buffer);
	glGenBuffers(1, &ground_normal_buffer);
	glGenBuffers(1, &ground_uv_buffer);

	glBindBuffer(GL_ARRAY_BUFFER, ground_vertex_buffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec3) * ground_vertices.size(), &ground_vertices[0], GL_STATIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);

	glBindBuffer(GL_ARRAY_BUFFER, ground_normal_buffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec3) * ground_normals.size(), &ground_normals[0], GL_STATIC_DRAW);
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, 0);

	glBindBuffer(GL_ARRAY_BUFFER, ground_uv_buffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec2) * ground_uvs.size(), &ground_uvs[0], GL_STATIC_DRAW);
	glEnableVertexAttribArray(2);
	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 0, 0);

	GLuint ground_texture;
	glGenTextures(1, &ground_texture);
	glBindTexture(GL_TEXTURE_2D, ground_texture);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, ground_texture_width, ground_texture_height, 0, GL_RGBA, GL_UNSIGNED_BYTE, &ground_decoded_texture[0]);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

	glBindVertexArray(depth_quad_vertex_array_object);
	GLuint depth_quad_vertex_buffer, depth_quad_uv_buffer;
	glGenBuffers(1, &depth_quad_vertex_buffer);
	glGenBuffers(1, &depth_quad_uv_buffer);

	glBindBuffer(GL_ARRAY_BUFFER, depth_quad_vertex_buffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec2)* depth_quad_vertices.size(), &depth_quad_vertices[0], GL_STATIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, 0);

	glBindBuffer(GL_ARRAY_BUFFER, depth_quad_uv_buffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec2)* depth_quad_uvs.size(), &depth_quad_uvs[0], GL_STATIC_DRAW);
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, 0);

	GLuint depth_quad_program = LoadShaders("shadows/shaders/depth_quad.vertex", "shadows/shaders/depth_quad.fragment");

	GLuint shadowmap_program = LoadShaders("shadows/shaders/shadowmap.vertex", "shadows/shaders/shadowmap.fragment");
	GLint shadowmap_light_mvp_uniform_location = glGetUniformLocation(shadowmap_program, "light_mvp");

	// we use this to also render the ground!
	GLuint monkey_program = LoadShaders("shadows/shaders/monkey.vertex", "shadows/shaders/monkey.fragment");
	GLint model_matrix_uniform_location = glGetUniformLocation(monkey_program, "model");
	GLint model_view_matrix_uniform_location = glGetUniformLocation(monkey_program, "model_view");
	GLint model_view_projection_matrix_uniform_location = glGetUniformLocation(monkey_program, "model_view_projection");
	GLint light_direction_uniform_location = glGetUniformLocation(monkey_program, "light_direction");
	GLint diffuse_texture_uniform_location = glGetUniformLocation(monkey_program, "diffuse_texture");
	GLint light_mvp_uniform_location = glGetUniformLocation(monkey_program, "light_mvp");
	GLint shadowmap_texture_uniform_location = glGetUniformLocation(monkey_program, "shadowmap_texture");

	//create the shadow map framebuffer
	const unsigned int shadowmap_width = 1024;
	const unsigned int shadowmap_height = 1024;

	GLuint shadowmap_framebuffer;
	glGenFramebuffers(1, &shadowmap_framebuffer);

	GLuint shadowmap_depth_texture;
	glGenTextures(1, &shadowmap_depth_texture);
	glBindTexture(GL_TEXTURE_2D, shadowmap_depth_texture);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, shadowmap_width, shadowmap_height, 0, GL_DEPTH_COMPONENT, GL_FLOAT, nullptr);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

	glBindFramebuffer(GL_FRAMEBUFFER, shadowmap_framebuffer);
	glFramebufferTexture(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, shadowmap_depth_texture, 0);
	glDrawBuffer(GL_NONE);
	glReadBuffer(GL_NONE);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);



	glm::mat4 perspective_projection = glm::perspective(fov, aspect_ratio, 0.1f, 100.0f);
	glm::mat4 model_matrix = glm::mat4(1.0f);

	glm::mat4 light_projection_matrix = glm::ortho(-10.0f, 10.0f, -10.0f, 10.0f, 1.0f, 30.0f);

	do {
		float elapsed_time = (std::chrono::system_clock::now() - start_time).count() * 0.0000000001f;

		glm::vec4 light_direction_ = glm::rotate(glm::mat4(1.0f), 5 * elapsed_time, glm::vec3(0.0f, 1.0f, 0.0f))
			* glm::vec4(original_light_direction, 0.0f);
		glm::vec3 light_direction(light_direction_);

		handle_input(window, &world_state);

		// first render the scene from light source point of view into shadowmap depth buffer
		glEnable(GL_DEPTH_TEST);
		glBindFramebuffer(GL_FRAMEBUFFER, shadowmap_framebuffer);

		// Question: what does this line do?
		glViewport(0, 0, shadowmap_width, shadowmap_height);

		glClear(GL_DEPTH_BUFFER_BIT);

		// Question: what does glm::lookAt do?
		glm::mat4 light_view_matrix = glm::lookAt(-15.0f * light_direction, glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.0f, 1.0f, 0.0f));
		glm::mat4 light_mvp = light_projection_matrix * light_view_matrix;
		glUseProgram(shadowmap_program);
		glUniformMatrix4fv(shadowmap_light_mvp_uniform_location, 1, GL_FALSE, &light_mvp[0][0]);

		glBindVertexArray(monkey_vertex_array_object);
		glDrawArrays(GL_TRIANGLES, 0, monkey_vertices.size());

		glBindVertexArray(ground_vertex_array_object);
		glDrawArrays(GL_TRIANGLE_STRIP, 0, ground_vertices.size());



		// switch to the default fragmebuffer (index 0)
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
		glViewport(0, 0, window_width, window_height);
		glClearColor(1.0f, 0.0f, 1.0f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);


		glm::vec3 camera_right = glm::cross(world_state.camera_forward, world_state.camera_up);

		glm::mat4 view_translation_matrix = glm::translate(glm::mat4(1.0f), -world_state.camera_position);
		glm::mat4 view_rotation_matrix = glm::mat4(0.0f);

		view_rotation_matrix[0][0] = -camera_right[0];
		view_rotation_matrix[0][1] = -camera_right[1];
		view_rotation_matrix[0][2] = -camera_right[2];

		view_rotation_matrix[1][0] = world_state.camera_up[0];
		view_rotation_matrix[1][1] = world_state.camera_up[1];
		view_rotation_matrix[1][2] = world_state.camera_up[2];

		view_rotation_matrix[2][0] = -world_state.camera_forward[0];
		view_rotation_matrix[2][1] = -world_state.camera_forward[1];
		view_rotation_matrix[2][2] = -world_state.camera_forward[2];

		view_rotation_matrix[3][3] = 1.0f;

		glm::mat4 view_matrix = view_rotation_matrix * view_translation_matrix;
		glm::mat4 model_view_matrix = view_matrix * model_matrix;
		glm::mat4 model_view_projection_matrix = perspective_projection * model_view_matrix;


		glUseProgram(monkey_program);

		// Question: what does this line do?
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, monkey_texture);
		glUniform1i(diffuse_texture_uniform_location, 0);

		glActiveTexture(GL_TEXTURE1);
		glBindTexture(GL_TEXTURE_2D, shadowmap_depth_texture);
		// Question: what does the 1 in the following function call mean?
		glUniform1i(shadowmap_texture_uniform_location, 1);

		glUniform3fv(light_direction_uniform_location, 1, &light_direction[0]);
		glUniformMatrix4fv(model_matrix_uniform_location, 1, GL_FALSE, &model_matrix[0][0]);
		glUniformMatrix4fv(model_view_matrix_uniform_location, 1, GL_FALSE, &model_view_matrix[0][0]);
		glUniformMatrix4fv(model_view_projection_matrix_uniform_location, 1, GL_FALSE, &model_view_projection_matrix[0][0]);
		glUniformMatrix4fv(light_mvp_uniform_location, 1, GL_FALSE, &light_mvp[0][0]);
		glBindVertexArray(monkey_vertex_array_object);
		glDrawArrays(GL_TRIANGLES, 0, monkey_vertices.size());

		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, ground_texture);
		glUniform1i(diffuse_texture_uniform_location, 0);

		glActiveTexture(GL_TEXTURE1);
		glBindTexture(GL_TEXTURE_2D, shadowmap_depth_texture);
		glUniform1i(shadowmap_texture_uniform_location, 1);

		// most of these uniforms are already set and don't need re-setting.
		// we only do it for the sake of simplicity
		glUniform3fv(light_direction_uniform_location, 1, &light_direction[0]);
		glUniformMatrix4fv(model_matrix_uniform_location, 1, GL_FALSE, &model_matrix[0][0]);
		glUniformMatrix4fv(model_view_matrix_uniform_location, 1, GL_FALSE, &model_view_matrix[0][0]);
		glUniformMatrix4fv(model_view_projection_matrix_uniform_location, 1, GL_FALSE, &model_view_projection_matrix[0][0]);
		glUniformMatrix4fv(light_mvp_uniform_location, 1, GL_FALSE, &light_mvp[0][0]);
		glBindVertexArray(ground_vertex_array_object);
		glDrawArrays(GL_TRIANGLE_STRIP, 0, ground_vertices.size());

		if (world_state.should_show_shadowmap){
			// we draw the UI on top of everything else so we don't need depth test
			glDisable(GL_DEPTH_TEST);

			glUseProgram(depth_quad_program);
			glActiveTexture(GL_TEXTURE0);
			glBindTexture(GL_TEXTURE_2D, shadowmap_depth_texture);
			glBindVertexArray(depth_quad_vertex_array_object);
			glDrawArrays(GL_TRIANGLE_STRIP, 0, depth_quad_vertices.size());
		}

		glfwSwapBuffers(window);
		glfwPollEvents();

	} while (glfwGetKey(window, GLFW_KEY_ESCAPE) != GLFW_PRESS && glfwWindowShouldClose(window) == 0);

	return 0;
}